Sys.time()

class(Sys.time())

as.POSIXct("2016-10-18 12:00:00")

as.POSIXct("2016-10-18 12:00:00", "UTC")

this_morn <- as.POSIXct("2016-10-18 7:00:00", "UTC")

tomorrow_morn <- this_morning + (60 * 60 * 24)

seq(this_morn, tomorrow_morn, by = "1 min")

as.POSIXct("2016.10.18 12:00:00")

as.POSIXct("10.18.2016 12:00:00", format = "%m.%d.%Y %H:%M:%S")

###############################################################################
###############################################################################
###############################################################################
