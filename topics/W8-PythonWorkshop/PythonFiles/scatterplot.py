# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt

points = np.random.uniform(-1,1,size=(250,2))

plt.scatter(points[:,0],points[:,1])
plt.title('Scatter Plot Example')