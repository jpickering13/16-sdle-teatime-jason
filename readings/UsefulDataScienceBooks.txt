Useful Data Science books
--------------

Elizabeth Matsui, and Roger D. Peng. The Art of Data Science. Leanpub, 2015. https://leanpub.com/artofdatascience.

Jeffrey Leek. The Elements of Data Analytic Style. Leanpub, 2014. https://leanpub.com/datastyle.

Roger D. Peng. Exploratory Data Analysis with R. Leanpub, 2015. https://leanpub.com/exdata.

Roger D. Peng. R Programming for Data Science. Leanpub, 2014. https://leanpub.com/rprogramming.

Roger D. Peng. Report Writing for Data Science in R. Leanpub, 2015. https://leanpub.com/reportwriting.

David M. Diez, Christopher D. Barr, and Mine Çetinkaya-Rundel. OpenIntro Statistics: Third Edition. 3 edition. S.l.: OpenIntro, Inc., 2015. https://www.openintro.org/stat/textbook.php?stat_book=os.

James, Gareth, Daniela Witten, Trevor Hastie, and Robert Tibshirani. An Introduction to Statistical Learning: With Applications in R. 1st ed. 2013, Corr. 5th printing 2015 edition. Springer Texts in Statistics. New York: Springer, 2013. http://www-bcf.usc.edu/~gareth/ISL/index.html.

